--
-- PostgreSQL database dump
--

-- Dumped from database version 14.0
-- Dumped by pg_dump version 14.0

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: bds; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA bds;


ALTER SCHEMA bds OWNER TO postgres;

--
-- Name: SCHEMA bds; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA bds IS 'standard public schema';


--
-- Name: bds-private; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA "bds-private";


ALTER SCHEMA "bds-private" OWNER TO postgres;

--
-- Name: adminpack; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION adminpack; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA bds;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: unaccent; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA bds;


--
-- Name: EXTENSION unaccent; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION unaccent IS 'text search dictionary that removes accents';


--
-- Name: celebration(); Type: FUNCTION; Schema: bds; Owner: postgres
--

CREATE FUNCTION bds.celebration() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
RAISE NOTICE 'Nazdar chlape!';
RETURN NEW;
END;
$$;


ALTER FUNCTION bds.celebration() OWNER TO postgres;

--
-- Name: insert_proc(integer, character varying, character varying, character varying, character varying); Type: PROCEDURE; Schema: bds; Owner: postgres
--

CREATE PROCEDURE bds.insert_proc(IN integer, IN character varying, IN character varying, IN character varying, IN character varying)
    LANGUAGE plpgsql
    AS $_$	
begin

insert into "Employee" (employee_id,first_name,last_name,mail,phone) values ($1,$2,$3,$4,$5);
commit;

end;
$_$;


ALTER PROCEDURE bds.insert_proc(IN integer, IN character varying, IN character varying, IN character varying, IN character varying) OWNER TO postgres;

--
-- Name: warning1(); Type: PROCEDURE; Schema: bds; Owner: postgres
--

CREATE PROCEDURE bds.warning1()
    LANGUAGE plpgsql
    AS $$
DECLARE us_id integer;
BEGIN
for us_id in (SELECT u."status_id" FROM 
 "Car_status" u RIGHT JOIN "Car_checkout" s
 ON u.status_id = s.status_id WHERE s.checkout_date = not null)
 LOOP
 RAISE NOTICE 
'Subscription of user with id % is about to expire in less than a week.', us_id;
 END LOOP;
END;
$$;


ALTER PROCEDURE bds.warning1() OWNER TO postgres;

--
-- Name: warning_checkout(); Type: PROCEDURE; Schema: bds; Owner: postgres
--

CREATE PROCEDURE bds.warning_checkout()
    LANGUAGE plpgsql
    AS $$

BEGIN
SELECT u.status_id, u.car_id, s.checkout_date FROM 
 "Car_status" u RIGHT JOIN "Car_checkout" s
 ON u.status_id = s.status_id WHERE u.status = 'used';
 RAISE NOTICE 
'This car needs to be checked';
 
END;
$$;


ALTER PROCEDURE bds.warning_checkout() OWNER TO postgres;

--
-- Name: warning_checkout1(); Type: PROCEDURE; Schema: bds; Owner: postgres
--

CREATE PROCEDURE bds.warning_checkout1()
    LANGUAGE plpgsql
    AS $$


DECLARE cars_id integer;
BEGIN
for cars_id in (SELECT u.status_id, u.car_id, s.checkout_date FROM 
 "Car_status" u RIGHT JOIN "Car_checkout" s
 ON u.status_id = s.status_id WHERE u.status = 'used')
 LOOP
 RAISE NOTICE 
'This car needs to be checked';
END LOOP;
 
END;
$$;


ALTER PROCEDURE bds.warning_checkout1() OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: Car_checkout; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Car_checkout" (
    checkout_id integer NOT NULL,
    status_id integer NOT NULL,
    checkout_date date NOT NULL
);


ALTER TABLE bds."Car_checkout" OWNER TO postgres;

--
-- Name: Car_checkout_checkout_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Car_checkout_checkout_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Car_checkout_checkout_id_seq" OWNER TO postgres;

--
-- Name: Car_checkout_checkout_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Car_checkout_checkout_id_seq" OWNED BY bds."Car_checkout".checkout_id;


--
-- Name: Car_status; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Car_status" (
    status_id integer NOT NULL,
    price integer NOT NULL,
    status character varying(20) NOT NULL,
    car_id integer NOT NULL
);


ALTER TABLE bds."Car_status" OWNER TO postgres;

--
-- Name: Car_status_status_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Car_status_status_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Car_status_status_id_seq" OWNER TO postgres;

--
-- Name: Car_status_status_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Car_status_status_id_seq" OWNED BY bds."Car_status".status_id;


--
-- Name: Company_car; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Company_car" (
    car_id integer NOT NULL,
    type character varying(20) NOT NULL
);


ALTER TABLE bds."Company_car" OWNER TO postgres;

--
-- Name: Company_car_car_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Company_car_car_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Company_car_car_id_seq" OWNER TO postgres;

--
-- Name: Company_car_car_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Company_car_car_id_seq" OWNED BY bds."Company_car".car_id;


--
-- Name: Customer; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Customer" (
    customer_id integer NOT NULL,
    first_name character varying(20) NOT NULL,
    last_name character varying(20) NOT NULL,
    age character varying(45)
);


ALTER TABLE bds."Customer" OWNER TO postgres;

--
-- Name: Customer_contact; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Customer_contact" (
    contact_id integer NOT NULL,
    customer_id integer NOT NULL,
    mail character varying(45) NOT NULL,
    phone character varying(45) NOT NULL
);


ALTER TABLE bds."Customer_contact" OWNER TO postgres;

--
-- Name: Customer_contact_contact_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Customer_contact_contact_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Customer_contact_contact_id_seq" OWNER TO postgres;

--
-- Name: Customer_contact_contact_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Customer_contact_contact_id_seq" OWNED BY bds."Customer_contact".contact_id;


--
-- Name: Customer_customer_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Customer_customer_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Customer_customer_id_seq" OWNER TO postgres;

--
-- Name: Customer_customer_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Customer_customer_id_seq" OWNED BY bds."Customer".customer_id;


--
-- Name: Customer_has_adress; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Customer_has_adress" (
    adress_id integer NOT NULL,
    customer_id integer NOT NULL
);


ALTER TABLE bds."Customer_has_adress" OWNER TO postgres;

--
-- Name: Customer_has_order; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Customer_has_order" (
    order_id integer NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE bds."Customer_has_order" OWNER TO postgres;

--
-- Name: Dummy ; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Dummy " (
);


ALTER TABLE bds."Dummy " OWNER TO postgres;

--
-- Name: Employee; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Employee" (
    employee_id integer NOT NULL,
    first_name character varying(20) NOT NULL,
    last_name character varying(20) NOT NULL,
    mail character varying(45) NOT NULL,
    phone character varying(45) NOT NULL,
    pwd text
);


ALTER TABLE bds."Employee" OWNER TO postgres;

--
-- Name: Employee_employee_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Employee_employee_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Employee_employee_id_seq" OWNER TO postgres;

--
-- Name: Employee_employee_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Employee_employee_id_seq" OWNED BY bds."Employee".employee_id;


--
-- Name: Employee_has_training; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Employee_has_training" (
    training_id integer NOT NULL,
    employee_id integer NOT NULL
);


ALTER TABLE bds."Employee_has_training" OWNER TO postgres;

--
-- Name: Employee_role; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Employee_role" (
    employee_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE bds."Employee_role" OWNER TO postgres;

--
-- Name: Employee_training; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Employee_training" (
    training_id integer NOT NULL,
    theme character varying(45) NOT NULL,
    training_date date NOT NULL,
    training_start time without time zone NOT NULL,
    training_end time without time zone NOT NULL
);


ALTER TABLE bds."Employee_training" OWNER TO postgres;

--
-- Name: Employee_training_training_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Employee_training_training_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Employee_training_training_id_seq" OWNER TO postgres;

--
-- Name: Employee_training_training_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Employee_training_training_id_seq" OWNED BY bds."Employee_training".training_id;


--
-- Name: Order; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Order" (
    order_id integer NOT NULL,
    customer_id integer NOT NULL,
    order_status character varying(20) NOT NULL,
    date date NOT NULL
);


ALTER TABLE bds."Order" OWNER TO postgres;

--
-- Name: Product; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Product" (
    product_id integer NOT NULL,
    product_name character varying(30) NOT NULL,
    price integer NOT NULL
);


ALTER TABLE bds."Product" OWNER TO postgres;

--
-- Name: Everything_from_order; Type: MATERIALIZED VIEW; Schema: bds; Owner: postgres
--

CREATE MATERIALIZED VIEW bds."Everything_from_order" AS
 SELECT t1.first_name,
    t1.last_name,
    t2.product_id,
    t3.order_status,
    t4.product_name,
    t4.price
   FROM (((bds."Order" t3
     JOIN bds."Customer_has_order" t2 ON ((t3.order_id = t2.order_id)))
     JOIN bds."Customer" t1 ON ((t1.customer_id = t3.customer_id)))
     JOIN bds."Product" t4 ON ((t4.product_id = t2.product_id)))
  WHERE ((t1.first_name IS NULL) OR ((t1.first_name)::text ~~ 'H%'::text))
  GROUP BY t1.first_name, t4.price, t2.product_id, t1.last_name, t3.order_status, t3.order_id, t4.product_name
 HAVING (t3.order_id < 15)
  WITH NO DATA;


ALTER TABLE bds."Everything_from_order" OWNER TO postgres;

--
-- Name: Manager; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Manager" (
    manager_id integer NOT NULL,
    first_name character varying(20) NOT NULL,
    last_name character varying(20) NOT NULL,
    mail character varying(45) NOT NULL,
    phone character varying(45) NOT NULL,
    manager_tier character varying(45) NOT NULL,
    car_id integer
);


ALTER TABLE bds."Manager" OWNER TO postgres;

--
-- Name: Manager_Employees; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Manager_Employees" (
    manager_id integer NOT NULL,
    employee_id integer NOT NULL
);


ALTER TABLE bds."Manager_Employees" OWNER TO postgres;

--
-- Name: Manager_has_role; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Manager_has_role" (
    manager_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE bds."Manager_has_role" OWNER TO postgres;

--
-- Name: Manager_manager_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Manager_manager_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Manager_manager_id_seq" OWNER TO postgres;

--
-- Name: Manager_manager_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Manager_manager_id_seq" OWNED BY bds."Manager".manager_id;


--
-- Name: Order_order_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Order_order_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Order_order_id_seq" OWNER TO postgres;

--
-- Name: Order_order_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Order_order_id_seq" OWNED BY bds."Order".order_id;


--
-- Name: Product_product_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Product_product_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Product_product_id_seq" OWNER TO postgres;

--
-- Name: Product_product_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Product_product_id_seq" OWNED BY bds."Product".product_id;


--
-- Name: Role; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Role" (
    role_id integer NOT NULL,
    role_type character varying(45) NOT NULL
);


ALTER TABLE bds."Role" OWNER TO postgres;

--
-- Name: Role_role_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Role_role_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Role_role_id_seq" OWNER TO postgres;

--
-- Name: Role_role_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Role_role_id_seq" OWNED BY bds."Role".role_id;


--
-- Name: Text; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Text" (
    id integer NOT NULL,
    popis text NOT NULL
);


ALTER TABLE bds."Text" OWNER TO postgres;

--
-- Name: Text_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Text_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Text_id_seq" OWNER TO postgres;

--
-- Name: Text_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Text_id_seq" OWNED BY bds."Text".id;


--
-- Name: Vyjezd; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds."Vyjezd" (
    vyjezd_id integer NOT NULL,
    "time" timestamp without time zone NOT NULL,
    car_id integer
);


ALTER TABLE bds."Vyjezd" OWNER TO postgres;

--
-- Name: Vyjezd_vyjezd_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds."Vyjezd_vyjezd_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds."Vyjezd_vyjezd_id_seq" OWNER TO postgres;

--
-- Name: Vyjezd_vyjezd_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds."Vyjezd_vyjezd_id_seq" OWNED BY bds."Vyjezd".vyjezd_id;


--
-- Name: adress; Type: TABLE; Schema: bds; Owner: postgres
--

CREATE TABLE bds.adress (
    adress_id integer NOT NULL,
    city character varying(45) NOT NULL,
    street character varying(45) NOT NULL,
    house_number character varying(45),
    zip_code character varying(45)
);


ALTER TABLE bds.adress OWNER TO postgres;

--
-- Name: adress_adress_id_seq; Type: SEQUENCE; Schema: bds; Owner: postgres
--

CREATE SEQUENCE bds.adress_adress_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bds.adress_adress_id_seq OWNER TO postgres;

--
-- Name: adress_adress_id_seq; Type: SEQUENCE OWNED BY; Schema: bds; Owner: postgres
--

ALTER SEQUENCE bds.adress_adress_id_seq OWNED BY bds.adress.adress_id;


--
-- Name: all_orders; Type: VIEW; Schema: bds; Owner: postgres
--

CREATE VIEW bds.all_orders AS
 SELECT t1.first_name,
    t1.last_name,
    t2.product_id,
    t3.order_status
   FROM ((bds."Order" t3
     JOIN bds."Customer_has_order" t2 ON ((t3.order_id = t2.order_id)))
     JOIN bds."Customer" t1 ON ((t1.customer_id = t3.customer_id)));


ALTER TABLE bds.all_orders OWNER TO postgres;

--
-- Name: number_of_people; Type: VIEW; Schema: bds; Owner: postgres
--

CREATE VIEW bds.number_of_people AS
 SELECT count(*) AS count
   FROM bds."Employee";


ALTER TABLE bds.number_of_people OWNER TO postgres;

--
-- Name: number_of_things; Type: VIEW; Schema: bds; Owner: postgres
--

CREATE VIEW bds.number_of_things AS
 SELECT count(*) AS count
   FROM bds."Employee",
    bds."Manager",
    bds."Customer",
    bds."Company_car",
    bds."Order" "number of orders";


ALTER TABLE bds.number_of_things OWNER TO postgres;

--
-- Name: teachers_view_on_employee; Type: VIEW; Schema: bds; Owner: postgres
--

CREATE VIEW bds.teachers_view_on_employee AS
 SELECT "Employee".employee_id,
    "Employee".first_name,
    "Employee".last_name,
    "Employee".mail
   FROM bds."Employee";


ALTER TABLE bds.teachers_view_on_employee OWNER TO postgres;

--
-- Name: Dummy; Type: TABLE; Schema: bds-private; Owner: postgres
--

CREATE TABLE "bds-private"."Dummy" (
);


ALTER TABLE "bds-private"."Dummy" OWNER TO postgres;

--
-- Name: Manager; Type: TABLE; Schema: bds-private; Owner: postgres
--

CREATE TABLE "bds-private"."Manager" (
    manager_id integer NOT NULL,
    first_name character varying(20) NOT NULL,
    last_name character varying(20) NOT NULL
);


ALTER TABLE "bds-private"."Manager" OWNER TO postgres;

--
-- Name: Manager_manager_id_seq; Type: SEQUENCE; Schema: bds-private; Owner: postgres
--

CREATE SEQUENCE "bds-private"."Manager_manager_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "bds-private"."Manager_manager_id_seq" OWNER TO postgres;

--
-- Name: Manager_manager_id_seq; Type: SEQUENCE OWNED BY; Schema: bds-private; Owner: postgres
--

ALTER SEQUENCE "bds-private"."Manager_manager_id_seq" OWNED BY "bds-private"."Manager".manager_id;


--
-- Name: Manager_pay; Type: TABLE; Schema: bds-private; Owner: postgres
--

CREATE TABLE "bds-private"."Manager_pay" (
    pay_id integer NOT NULL,
    manager_id integer NOT NULL,
    pay integer NOT NULL
);


ALTER TABLE "bds-private"."Manager_pay" OWNER TO postgres;

--
-- Name: Manager_pay_pay_id_seq; Type: SEQUENCE; Schema: bds-private; Owner: postgres
--

CREATE SEQUENCE "bds-private"."Manager_pay_pay_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "bds-private"."Manager_pay_pay_id_seq" OWNER TO postgres;

--
-- Name: Manager_pay_pay_id_seq; Type: SEQUENCE OWNED BY; Schema: bds-private; Owner: postgres
--

ALTER SEQUENCE "bds-private"."Manager_pay_pay_id_seq" OWNED BY "bds-private"."Manager_pay".pay_id;


--
-- Name: Reservation; Type: TABLE; Schema: bds-private; Owner: postgres
--

CREATE TABLE "bds-private"."Reservation" (
    reservation_id integer NOT NULL,
    reservationtext character varying(45) NOT NULL,
    reservationname character varying(10)
);


ALTER TABLE "bds-private"."Reservation" OWNER TO postgres;

--
-- Name: Reservation_reservation_id_seq; Type: SEQUENCE; Schema: bds-private; Owner: postgres
--

CREATE SEQUENCE "bds-private"."Reservation_reservation_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE "bds-private"."Reservation_reservation_id_seq" OWNER TO postgres;

--
-- Name: Reservation_reservation_id_seq; Type: SEQUENCE OWNED BY; Schema: bds-private; Owner: postgres
--

ALTER SEQUENCE "bds-private"."Reservation_reservation_id_seq" OWNED BY "bds-private"."Reservation".reservation_id;


--
-- Name: Car_checkout checkout_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Car_checkout" ALTER COLUMN checkout_id SET DEFAULT nextval('bds."Car_checkout_checkout_id_seq"'::regclass);


--
-- Name: Car_status status_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Car_status" ALTER COLUMN status_id SET DEFAULT nextval('bds."Car_status_status_id_seq"'::regclass);


--
-- Name: Company_car car_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Company_car" ALTER COLUMN car_id SET DEFAULT nextval('bds."Company_car_car_id_seq"'::regclass);


--
-- Name: Customer customer_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer" ALTER COLUMN customer_id SET DEFAULT nextval('bds."Customer_customer_id_seq"'::regclass);


--
-- Name: Customer_contact contact_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer_contact" ALTER COLUMN contact_id SET DEFAULT nextval('bds."Customer_contact_contact_id_seq"'::regclass);


--
-- Name: Employee employee_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Employee" ALTER COLUMN employee_id SET DEFAULT nextval('bds."Employee_employee_id_seq"'::regclass);


--
-- Name: Employee_training training_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Employee_training" ALTER COLUMN training_id SET DEFAULT nextval('bds."Employee_training_training_id_seq"'::regclass);


--
-- Name: Manager manager_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Manager" ALTER COLUMN manager_id SET DEFAULT nextval('bds."Manager_manager_id_seq"'::regclass);


--
-- Name: Order order_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Order" ALTER COLUMN order_id SET DEFAULT nextval('bds."Order_order_id_seq"'::regclass);


--
-- Name: Product product_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Product" ALTER COLUMN product_id SET DEFAULT nextval('bds."Product_product_id_seq"'::regclass);


--
-- Name: Role role_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Role" ALTER COLUMN role_id SET DEFAULT nextval('bds."Role_role_id_seq"'::regclass);


--
-- Name: Text id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Text" ALTER COLUMN id SET DEFAULT nextval('bds."Text_id_seq"'::regclass);


--
-- Name: Vyjezd vyjezd_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Vyjezd" ALTER COLUMN vyjezd_id SET DEFAULT nextval('bds."Vyjezd_vyjezd_id_seq"'::regclass);


--
-- Name: adress adress_id; Type: DEFAULT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds.adress ALTER COLUMN adress_id SET DEFAULT nextval('bds.adress_adress_id_seq'::regclass);


--
-- Name: Manager manager_id; Type: DEFAULT; Schema: bds-private; Owner: postgres
--

ALTER TABLE ONLY "bds-private"."Manager" ALTER COLUMN manager_id SET DEFAULT nextval('"bds-private"."Manager_manager_id_seq"'::regclass);


--
-- Name: Manager_pay pay_id; Type: DEFAULT; Schema: bds-private; Owner: postgres
--

ALTER TABLE ONLY "bds-private"."Manager_pay" ALTER COLUMN pay_id SET DEFAULT nextval('"bds-private"."Manager_pay_pay_id_seq"'::regclass);


--
-- Name: Reservation reservation_id; Type: DEFAULT; Schema: bds-private; Owner: postgres
--

ALTER TABLE ONLY "bds-private"."Reservation" ALTER COLUMN reservation_id SET DEFAULT nextval('"bds-private"."Reservation_reservation_id_seq"'::regclass);


--
-- Data for Name: Car_checkout; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Car_checkout" (checkout_id, status_id, checkout_date) FROM stdin;
1	1	2022-10-10
2	2	2022-01-20
3	3	2022-06-10
\.


--
-- Data for Name: Car_status; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Car_status" (status_id, price, status, car_id) FROM stdin;
1	350000	new	1
2	500000	Used	3
3	600000	Basically new	2
\.


--
-- Data for Name: Company_car; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Company_car" (car_id, type) FROM stdin;
1	Skoda Octavia
2	Skoda Superb
3	VW Passat
\.


--
-- Data for Name: Customer; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Customer" (customer_id, first_name, last_name, age) FROM stdin;
1	Helga	Socorro	25
2	Photine	Trine	40
3	Helena	Mbalenhle	39
4	Milena	Gwendolyn	29
5	Gayane	Priyanka	17
6	Fikret	Treasach	55
7	Alexander	Mamadu	42
8	Khaleel	Septimius	19
9	Ratko	Opeyemi	21
10	Carbrey	Vígi	26
\.


--
-- Data for Name: Customer_contact; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Customer_contact" (contact_id, customer_id, mail, phone) FROM stdin;
1	10	Vigo19@yahoo.com	902733737
2	1	helgasoccer@yahoo.com	578630063
3	2	Photluntho@yahoo.com	326303701
4	3	helena1999@yahoo.com	949863980
5	4	Gwendoline55@yahoo.com	070514221
6	5	gayaprina@yahoo.com	047200033
7	6	fikrettree@yahoo.com	228231178
8	7	alexmamad@yahoo.com	495336064
9	8	SeptimusVarus@yahoo.com	662528220
10	9	elRatko@yahoo.com	719129379
\.


--
-- Data for Name: Customer_has_adress; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Customer_has_adress" (adress_id, customer_id) FROM stdin;
5	3
5	4
2	1
3	2
4	5
6	6
7	7
8	8
1	9
9	10
\.


--
-- Data for Name: Customer_has_order; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Customer_has_order" (order_id, product_id) FROM stdin;
2	3
3	10
54	11
55	13
56	5
57	6
58	7
59	10
60	10
61	12
62	13
63	14
\.


--
-- Data for Name: Dummy ; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Dummy "  FROM stdin;
\.


--
-- Data for Name: Employee; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Employee" (employee_id, first_name, last_name, mail, phone, pwd) FROM stdin;
2	Ivo	Meirion	ivo.meirion@gmail.com	700581159	$2a$06$49xaM6sHVYl00UCOzP91f.C38R77EaPHvE3qIcLCNkgqktAIHuGXS
3	Olivier	Krischna	Krishna.olivier@gmail.com	616133588	$2a$06$tjhgyQc31mCSBh4nhFmCyuhUECSFbMEOOoBL0oDRL.z0zBjXJqdFq
4	Farhata	Gervasio	Gervasio.farhata@gmail.com	456723865	$2a$06$vzJSxFpVanILYs/TDC47Tu1z.ioXSiTDn8Xb7xdzWeXAQ3KNWD2Na
5	Aníbal	Prymr	Aníbal.Prymr@gmail.com	485387551	$2a$06$gGvWrRkrt.spm/40wN06NOfibxMH6VC4YV6sB2JsgGN2Ld9WP9ddm
6	Nereus	Jezza	Nereus.jezza@gmail.com	107649444	$2a$06$dYU.QAqqYVvslXdC0WJo3e2ACOsxFQ/8wtndQ.A19K3glu19vLVM.
7	Armjun	Lemoine	armjun.lemoine@gmail.com	425283641	$2a$06$PTh3xjOz7eQRPAsTkDtfHuGh2m5rq.RWX4gJYWf996XsBo1kxhuzW
8	Pam	Hemming	PHemming@gmail.com	797666022	$2a$06$g2NZ.dyYC/jYZP.H2eySP.P8cm9X5MuYexXkn2OjEJSddiqisrNNO
9	Gotam	Svatopluk	GottemS@gmail.com	243536780	$2a$06$buxsFcrC6HcBquIt7Z4kiOinppmljFeT4cXIjvqHk6e6S.ycox/DO
10	Unathi	Gathelord	Gathe65@yahoo.com	844373647	$2a$06$.GDZ7tPxefjqDerVDqh8HunWil7cJH4NGkr4fCnSWW8a8SseWYvPS
11	Leofdaeg	Ganzorig	Lampganz@yahoo.com	957613326	$2a$06$V7Hl1OdOkZavbjls9MkMYeH6wFzBJS2dGrSItAcuALeiWwItyMlzu
12	Carl	Phokas	CarlosVemolos@yahoo.com	289533112	$2a$06$2Lot25YLUDovQVqUYjk3E.MrQ6N0bO4F5KWMUnAkJRyNZK.E00Nrq
13	Chad	Thunder	chadthundercock@yahoo.com	306843217	$2a$06$YtkULSWUKT7qhT.9Ae//vuf1lvNvrVM0X5DbcUqcDzjchoem4B2PW
14	Pipin	Zulfaraq	Zulgurub@yahoo.com	965737170	$2a$06$IB58yjGz/qBKxVL7ogAvDecNFHRZKSEbYFUkUblodZI6fhcMW5aym
15	Chad	Aputsiaq	Aputsiaq.chad@yahoo.com	226270592	$2a$06$txDH4Z1oi0IY4Yue3k.a8uSwFXimdyLKe.7.pDa69VmqEW2VRFp9i
16	Kristopher	Octavian	OctavianKris@gmail.com	058742063	$2a$06$pWh55sb94.fhxYPNqzjQduTZA3NseLipgsysrK9se5FDArTap4QDy
17	Marcianno	Savvas	Marciann19@gmail.com	634208015	$2a$06$7LVVG2SaXQOBQzvDhe0V7eAt9EKrlai6kfdTjRNoc3dCBltTos48S
18	Tomislav	Milburn	Tommy.Millburn@gmail.com	982500559	$2a$06$mggGtlhZSqBI4dGp3X.X0ukN/DtMd43LWTh/.DCmIUlMnHxL3Mgry
19	Daniel	Nikanor	dan.nikanor@gmail.com	303178384	$2a$06$4Ob1iyb7609O8LmHc/PJM.r8ElZNUUKdiUxUAUDlDL4gH7R36Mm1m
20	Albinhav	Kazuo	Albin.kaz@gmail.com	510040942	$2a$06$TK2bx4p9I9sIDmqaaGQHlOicn.6jcQpjVnZko3hf2q2ttXayxuLvK
21	Amel	Teodosio	Amelosak@gmail.com	413763829	$2a$06$xgj5SxvqbreRCzEzUaVqsOtwDtb2qAnorJXvHOtAwU0SKcH2lTloC
22	Dragisha	Anakoni	Dragonanak@gmail.com	029346184	$2a$06$zF46FjWqR7AtxmJBpJ6n/.LGaLDx36lAISzrXsdNEb6yPGFkqt8Hi
23	Vital	Wislal	Vitality@gmail.com	508612707	$2a$06$ZfofQ7AouELT3yduwnl3Vu2URW7FkSkEuNwC/xQqg4JaiukceUCVi
24	Bagomil	Anupal	Anupal654@gmail.com	520897289	$2a$06$54c4kQXo/58CwQvsO7eEHe0SsdQ/9Q5wSuCT70DJQOR9aIhu1Dqqy
25	Fawzi	Chikondi	Chikondifaw@gmail.com	604539257	$2a$06$Z9vJ2wHp6/U7p7DrNQRZD.G/dmAMqAuK.plFoCQPROtblr/vGKtKy
1	Daniel	Suchy	daniel.suchy@email.cz	7234479489	$2a$06$kOXDwhwD8/tMzYZPXp689u6u62YXQA.EI18zkqQNwh4BQARCQfqqm
26	Einion	Sabbas	Einionsabbas@gmail.com	238109470	$2a$06$yp8E.fS42B1zKpbqyHRKGe9q9QOsWmmbM7ojgLD2XWF7czcSghe7m
27	Arne	Liupold	Arnelupi5@gmail.com	653743561	$2a$06$VrsTe23XFL.2AgxmDZN9VeHhzI3E04vhFLvEOw4W4gL4M5X1oWHsS
28	Annani	Abdulla	Abdulla.annani4@gmail.com	884348364	$2a$06$mlTecNNfACLtLPEnPf/lPOfolc2Bbks2u5YUncXwY3aj1iw67EoG6
29	Nikita	Sepeteus	Nikita6589@gmail.com	474384221	$2a$06$YAy0SJ8.yqySgEUidXVkjeJOe/auQuic2yjD0y6WEACrvx1z5P2H2
30	Roman	Vinay	romanblaster@gmail.com	489763110	$2a$06$jzZhyeP57qUqNOU0lqUp3.Pq0sNt.C/qcUMqvvTKhp0bltjY/8wNG
31	Vlastimil	Plaminek	jakoohinek@gmail.com	691025962	$2a$06$eOruM/jrvZJFmt9/HAvppOon7ryS.1rLbMjZSdodaNsD/inDfruuu
32	Lukas	Zitnik	soyginger@gmail.com	671000533	$2a$06$7ScYOCFrA6wC1y9HEka65O.OBSZDF2AuIWJnv24HrBfSw6D/Qcyja
33	Noe	Theoderich	noesavior594@gmail.com	675551962	$2a$06$HEjuzT.aRVigi0ZRh6PVxuzM3vrXlMyNo02sfkKGq6MvM/wP0wKxC
34	Dominik	Jansa	janzic7@gmail.com	988191697	$2a$06$oOUkoIwZU5Ihz1gUcnszs.XFo83m7oyUkzILMSOPMIYL51/21pX0a
35	Hovo	Fedelmid	h.fedel@gmail.com	446556600	$2a$06$aZVrL9dsYiXKvXj5wOd7rOFJXxyzovlAB/lwxcc6fguUV4zjuRJ92
36	Lei	Rajender	lei.rejender@gmail.com	777322429	$2a$06$Ml2psmcnheM9EdJRefaXeuVhMcNwiubsiAW/t2nkDtbfe8xhmFOfW
37	Jarome	Jasir	JaromeJas@gmail.com	196006265	$2a$06$57HsDqjbnWfkWjPqqCMG7u7pV9vLRIEoDIagtbI5ej4p7q86Hsln6
38	Alexis	Phoibos	Alexis.gr@gmail.com	595250921	$2a$06$gaVfN8vSXzHCo.NbHMZPFucgmET5oJyhGEWCAW2RFNhPkgYjgpwMK
39	Eutropio	Aurelius	eu.aurol@gmail.com	196829172	$2a$06$Q7FCwIa0jra4tCvfLVlrzOvwqnsa5EryKoF2gV0VV/x0kGqKTP4aG
40	Antonello	Adnan	Tonny.adnan@gmail.com	530140776	$2a$06$MwIblLpaRFKoNk306seRhOIt.j7lzzVXlmJxEKsqLMwMyzd/e4tc6
41	Samet	Cynesige	samet.cyn65@gmail.com	823359441	$2a$06$ORmT3g1SRn4C0iL3g1a6V..1iBwoKaP3FV3E/pIQMs19gQBZ/jdmm
42	Anup	Kemp	Kemp.anup@gmail.com	435118135	$2a$06$hGFppJfPlDctBYK.2xh9aeWILiqgrBDCThzXbmya01C7pGtw0Ii9a
43	Bipin	Cuneyt	Cuneyt.wt@gmail.com	417522509	$2a$06$S2CgD6OhwiXYjcZBnBwQKeRBMcuw/qWdADtKloBKL7QJ8EMoozDPS
44	Lakshmi	Heidrich	Laksmana@gmail.com	475710755	$2a$06$5JrX2Qwniq0xK7WVvrvNs.jMIDbdbVl/sZDrNJf9Q/7TS4j2cdKDm
45	Titus	Piliriani	titus.pili@gmail.com	101723637	$2a$06$X94TQIAtwxmnnwq0jlfoiOghrAI37D23PEhcf8rDB5/yPD3YMlkBy
46	Ciriaco	Saiful	c.saiful@gmail.com	713475626	$2a$06$iC41a4/CUa0b5.0STBMS..MroWZES3.3omHDdYNWrKygu4MoE4gOC
47	Ricmod	Slava	slavia@gmail.com	404985051	$2a$06$QGfs0RX4Cze6MdHpf1GWcePrPae8fZ.rkUD6VpzJIdTXQKK85Na3y
48	Sten	Plutarch	stenplut@gmail.com	336518871	$2a$06$eJyokaT.zIPtksO4OfY5f.jZ6wTj6m99sJ56QRynaCWAFZOFhUk8K
49	David	Barta	bartasmile@gmail.com	987334807	$2a$06$hP9HSmkJLrJc3/CdQdkfouJaduqwsN7OvaKRV8jeeFGlezYTVDnr6
50	Max	Brazda	bmw@gmail.com	70056974	$2a$06$Je4BgItjDh3udGch.XhjZOSygLynFJdLY3aSKrAhYCdmIG46xASOa
51	Anastasia	Muriel	annamuri@gmail.com	234318217	$2a$06$gEHH71fi8txspynHBiJ9R.B.KmsXb7RBd7btwWWrCMh4gUZyDnB6W
52	Jacinda	Thandi	jac.thandi@gmail.com	476206726	$2a$06$MT1br0d1x3R68sW2mBAmbuDh0N2jPtT0/3NgILt2pJzkxmQztxtr6
53	Katrina	Yui	Katayu@yahoo.com	207118261	$2a$06$g445E2LyM2KzCac9Zd4O7.2L9OurcXI9lNmNhPQ60m/eNyTQ.Yv3C
54	Adelaine	Iliyana	Ilis@gmail.com	349771693	$2a$06$1hnsYbptFL.3IRC.GMW.ze0oYMMHL9d4TQlVSYdvP7HbSAJ9qPJlK
61	Test	Test	Test	Test	$2a$12$RoNiCq/SrJhGkzd6SQBgo.Fp28bblwTGhinR.e43yS9bQR.iYm/CK
\.


--
-- Data for Name: Employee_has_training; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Employee_has_training" (training_id, employee_id) FROM stdin;
5	1
5	2
5	3
5	4
5	5
5	6
5	7
5	8
5	9
5	10
5	11
5	12
5	13
5	14
5	15
5	16
5	17
5	18
5	19
5	20
5	21
5	22
5	23
5	24
5	25
5	26
5	27
5	28
5	29
5	30
5	31
5	32
5	33
5	34
5	35
5	36
5	37
5	38
5	39
5	40
5	41
5	42
5	43
5	44
5	45
5	46
5	47
5	48
5	49
5	50
5	51
5	52
5	53
5	54
6	1
6	2
6	3
6	4
6	5
6	6
6	7
6	8
6	9
6	10
6	11
6	12
6	13
6	14
6	15
6	16
6	17
6	18
6	19
6	20
6	21
6	22
6	23
6	24
6	25
6	26
6	27
6	28
6	29
6	30
6	31
6	32
6	33
6	34
6	35
6	36
6	37
6	38
6	39
6	40
6	41
6	42
6	43
6	44
6	45
6	46
6	47
6	48
6	49
6	50
6	51
6	52
6	53
6	54
4	26
4	27
4	28
4	29
4	30
4	31
4	32
4	33
4	34
4	35
4	36
3	1
3	2
3	3
3	4
3	5
3	6
3	7
3	8
3	9
3	10
3	11
3	12
3	13
3	14
3	15
3	16
3	17
3	18
3	19
3	20
3	21
3	22
3	23
3	24
3	25
2	1
2	2
2	3
2	4
2	5
2	6
2	7
2	8
2	9
2	10
2	11
2	12
2	13
2	14
2	15
2	16
2	17
2	18
2	19
2	20
2	21
2	22
2	23
2	24
2	25
2	26
2	27
2	28
2	29
2	30
2	31
2	32
2	33
2	34
2	35
2	36
2	37
2	38
2	39
2	40
2	41
2	42
2	43
2	44
2	45
2	46
2	47
2	48
2	49
2	50
2	51
2	52
2	53
2	54
\.


--
-- Data for Name: Employee_role; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Employee_role" (employee_id, role_id) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
13	1
14	1
15	1
16	1
17	1
18	1
19	1
20	1
21	1
22	1
23	1
24	1
25	1
26	2
27	2
28	2
29	2
30	2
31	2
32	2
33	2
34	2
35	2
36	2
37	10
38	10
39	7
40	4
41	4
42	5
43	5
44	5
45	5
46	6
47	6
48	6
49	3
50	3
51	9
52	8
53	8
54	8
\.


--
-- Data for Name: Employee_training; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Employee_training" (training_id, theme, training_date, training_start, training_end) FROM stdin;
2	New BOZP	2021-11-05	14:00:00	15:30:00
3	New machine	2021-11-10	10:00:00	12:00:00
4	Driving optimization	2021-11-20	14:00:00	16:00:00
5	Principles of work	2021-11-25	08:00:00	09:00:00
6	First aid training	2021-11-30	09:00:00	10:30:00
\.


--
-- Data for Name: Manager; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Manager" (manager_id, first_name, last_name, mail, phone, manager_tier, car_id) FROM stdin;
1	Mahavir	Varda	m.varda@gmail.com	088919239	1	1
2	Sara	Josephina	saraJ@gmail.com	811005132	2	1
3	Elli	Nanda	nanda.elli@gmail.com	465170690	3	2
4	Julius	Nikica	Jul.nikita@gmail.com	807609623	4	2
5	Ingomar	Odgjen	Iomar@yahoo.com	607582562	4	3
\.


--
-- Data for Name: Manager_Employees; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Manager_Employees" (manager_id, employee_id) FROM stdin;
4	1
4	2
4	3
4	4
4	5
4	6
4	7
4	8
4	9
4	10
4	11
4	12
4	13
4	14
4	15
4	16
4	17
4	18
4	19
4	20
4	21
4	22
4	23
4	24
4	25
5	26
5	27
5	28
5	29
5	30
5	31
5	32
5	33
5	34
5	35
5	36
4	42
4	43
4	44
4	45
4	40
4	41
4	39
3	37
3	38
3	51
3	52
3	53
3	54
3	46
3	47
3	48
3	49
3	50
\.


--
-- Data for Name: Manager_has_role; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Manager_has_role" (manager_id, role_id) FROM stdin;
4	15
1	11
2	12
3	13
5	17
\.


--
-- Data for Name: Order; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Order" (order_id, customer_id, order_status, date) FROM stdin;
2	1	Complete	2021-09-19
3	1	Out of stock	2020-09-19
54	2	Waiting for delivery	2021-10-29
55	2	Waiting for delivery	2021-10-30
56	3	From warehouse	2021-11-01
57	4	From warehouse	2021-10-31
58	5	From warehouse	2021-10-28
59	6	Waiting for delivery	2021-10-25
60	7	Waiting for delivery	2021-10-26
61	8	Local delivery	2021-10-31
62	9	Local delivery	2021-10-31
63	10	Local delivery	2021-10-27
\.


--
-- Data for Name: Product; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Product" (product_id, product_name, price) FROM stdin;
1	Printer	6500
2	Printer	6500
3	PC	30000
4	Microwave	2500
5	Toner	500
6	Mouse	1500
7	Keyboard	3000
8	Headphones	2500
10	HIFI Tower	4300
11	4K Television	14000
12	Digital Alarm	999
13	LCD monitor	3800
14	Fridge	18000
9	DVD	1900
\.


--
-- Data for Name: Role; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Role" (role_id, role_type) FROM stdin;
1	warehouseman
2	Driver
3	Doorkeeper
4	Part time worker
5	Packer
6	IT
7	Mechanic
8	Cleaning personnel
9	Secretary
10	Accountant
11	CEO
12	General manager
13	Functional manager
14	Frontline manager
15	Shift master
17	Driver shift master
\.


--
-- Data for Name: Text; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Text" (id, popis) FROM stdin;
1	Čermáková
\.


--
-- Data for Name: Vyjezd; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds."Vyjezd" (vyjezd_id, "time", car_id) FROM stdin;
1	2021-11-16 02:00:46.46325	1
2	2021-11-16 13:41:58.654281	2
3	2021-11-16 13:41:58.654281	3
\.


--
-- Data for Name: adress; Type: TABLE DATA; Schema: bds; Owner: postgres
--

COPY bds.adress (adress_id, city, street, house_number, zip_code) FROM stdin;
1	Brno	Bozetechova	65	60200
2	Lancaster	Lincoln drive	4938	17552
3	Amoret	Oakwood AV	3792	64722
4	Chicago	Whaley lane	4962	60606
5	Berlin	Genslerstraße	97	13405
6	Moosinning	Büsingstrasse	95	85452
7	FOLKINGHAM	Main Rd	88	NG34 9FQ
8	WOODPLUMPTON	Redcliffe Way	113	PR4 0YP
9	OSULLIVAN BEACH	Burnley Street	77	5166
\.


--
-- Data for Name: Dummy; Type: TABLE DATA; Schema: bds-private; Owner: postgres
--

COPY "bds-private"."Dummy"  FROM stdin;
\.


--
-- Data for Name: Manager; Type: TABLE DATA; Schema: bds-private; Owner: postgres
--

COPY "bds-private"."Manager" (manager_id, first_name, last_name) FROM stdin;
1	Mahavir	Varda
2	Sara	Josephina
3	Elli	Nanda
4	Julius	Nikica
5	Ingomar	Odgjen
\.


--
-- Data for Name: Manager_pay; Type: TABLE DATA; Schema: bds-private; Owner: postgres
--

COPY "bds-private"."Manager_pay" (pay_id, manager_id, pay) FROM stdin;
1	1	100000
2	2	150000
3	3	85000
4	4	60000
5	5	40000
\.


--
-- Data for Name: Reservation; Type: TABLE DATA; Schema: bds-private; Owner: postgres
--

COPY "bds-private"."Reservation" (reservation_id, reservationtext, reservationname) FROM stdin;
1	22.1.2022 => 13:00-14:00	\N
2	3.1.2022 => 13:00-14:00	\N
3	Test	Test
4	Test;" Drop table "bds-private"."Dummy";	
5	Test"; drop table "bds-private"."Dummy";	
6	Test; drop table "bds-private"."Dummy"; --"	
7	Test; DROP TABLE "bds-private"."Dummy";	
8	Test; drop table "bds-private"."Dummy"	
9	Test	
10	Test	
11	Test	
12	Test65	Test65 
13	20.1.2022	Chlapák
14	25.3.2022 => 08:00-12:00	Filip
\.


--
-- Name: Car_checkout_checkout_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Car_checkout_checkout_id_seq"', 3, true);


--
-- Name: Car_status_status_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Car_status_status_id_seq"', 3, true);


--
-- Name: Company_car_car_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Company_car_car_id_seq"', 3, true);


--
-- Name: Customer_contact_contact_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Customer_contact_contact_id_seq"', 10, true);


--
-- Name: Customer_customer_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Customer_customer_id_seq"', 10, true);


--
-- Name: Employee_employee_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Employee_employee_id_seq"', 66, true);


--
-- Name: Employee_training_training_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Employee_training_training_id_seq"', 6, true);


--
-- Name: Manager_manager_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Manager_manager_id_seq"', 7, true);


--
-- Name: Order_order_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Order_order_id_seq"', 63, true);


--
-- Name: Product_product_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Product_product_id_seq"', 14, true);


--
-- Name: Role_role_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Role_role_id_seq"', 17, true);


--
-- Name: Text_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Text_id_seq"', 1, true);


--
-- Name: Vyjezd_vyjezd_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds."Vyjezd_vyjezd_id_seq"', 4, true);


--
-- Name: adress_adress_id_seq; Type: SEQUENCE SET; Schema: bds; Owner: postgres
--

SELECT pg_catalog.setval('bds.adress_adress_id_seq', 9, true);


--
-- Name: Manager_manager_id_seq; Type: SEQUENCE SET; Schema: bds-private; Owner: postgres
--

SELECT pg_catalog.setval('"bds-private"."Manager_manager_id_seq"', 5, true);


--
-- Name: Manager_pay_pay_id_seq; Type: SEQUENCE SET; Schema: bds-private; Owner: postgres
--

SELECT pg_catalog.setval('"bds-private"."Manager_pay_pay_id_seq"', 5, true);


--
-- Name: Reservation_reservation_id_seq; Type: SEQUENCE SET; Schema: bds-private; Owner: postgres
--

SELECT pg_catalog.setval('"bds-private"."Reservation_reservation_id_seq"', 14, true);


--
-- Name: Car_checkout Car_checkout_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Car_checkout"
    ADD CONSTRAINT "Car_checkout_pkey" PRIMARY KEY (checkout_id);


--
-- Name: Car_status Car_status_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Car_status"
    ADD CONSTRAINT "Car_status_pkey" PRIMARY KEY (status_id);


--
-- Name: Company_car Company_car_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Company_car"
    ADD CONSTRAINT "Company_car_pkey" PRIMARY KEY (car_id);


--
-- Name: Customer_contact Customer_contact_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer_contact"
    ADD CONSTRAINT "Customer_contact_pkey" PRIMARY KEY (contact_id);


--
-- Name: Customer_has_adress Customer_has_adress_customer_id_key; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer_has_adress"
    ADD CONSTRAINT "Customer_has_adress_customer_id_key" UNIQUE (customer_id);


--
-- Name: Customer Customer_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer"
    ADD CONSTRAINT "Customer_pkey" PRIMARY KEY (customer_id);


--
-- Name: Employee Employee_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Employee"
    ADD CONSTRAINT "Employee_pkey" PRIMARY KEY (employee_id);


--
-- Name: Employee_training Employee_training_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Employee_training"
    ADD CONSTRAINT "Employee_training_pkey" PRIMARY KEY (training_id);


--
-- Name: Manager_has_role Manager_has_role_manager_id_key; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Manager_has_role"
    ADD CONSTRAINT "Manager_has_role_manager_id_key" UNIQUE (manager_id);


--
-- Name: Manager_has_role Manager_has_role_role_id_key; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Manager_has_role"
    ADD CONSTRAINT "Manager_has_role_role_id_key" UNIQUE (role_id);


--
-- Name: Manager Manager_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Manager"
    ADD CONSTRAINT "Manager_pkey" PRIMARY KEY (manager_id);


--
-- Name: Order Order_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Order"
    ADD CONSTRAINT "Order_pkey" PRIMARY KEY (order_id);


--
-- Name: Product Product_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Product"
    ADD CONSTRAINT "Product_pkey" PRIMARY KEY (product_id);


--
-- Name: Role Role_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Role"
    ADD CONSTRAINT "Role_pkey" PRIMARY KEY (role_id);


--
-- Name: Text Text_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Text"
    ADD CONSTRAINT "Text_pkey" PRIMARY KEY (id);


--
-- Name: Vyjezd Vyjezd_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Vyjezd"
    ADD CONSTRAINT "Vyjezd_pkey" PRIMARY KEY (vyjezd_id);


--
-- Name: adress adress_pkey; Type: CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds.adress
    ADD CONSTRAINT adress_pkey PRIMARY KEY (adress_id);


--
-- Name: Manager_pay Manager_pay_pkey; Type: CONSTRAINT; Schema: bds-private; Owner: postgres
--

ALTER TABLE ONLY "bds-private"."Manager_pay"
    ADD CONSTRAINT "Manager_pay_pkey" PRIMARY KEY (pay_id);


--
-- Name: Manager Manager_pkey; Type: CONSTRAINT; Schema: bds-private; Owner: postgres
--

ALTER TABLE ONLY "bds-private"."Manager"
    ADD CONSTRAINT "Manager_pkey" PRIMARY KEY (manager_id);


--
-- Name: Reservation Reservation_pkey; Type: CONSTRAINT; Schema: bds-private; Owner: postgres
--

ALTER TABLE ONLY "bds-private"."Reservation"
    ADD CONSTRAINT "Reservation_pkey" PRIMARY KEY (reservation_id);


--
-- Name: id_idx; Type: INDEX; Schema: bds; Owner: postgres
--

CREATE UNIQUE INDEX id_idx ON bds."Role" USING btree (role_type);


--
-- Name: Employee new_guy; Type: TRIGGER; Schema: bds; Owner: postgres
--

CREATE TRIGGER new_guy AFTER INSERT ON bds."Employee" FOR EACH ROW EXECUTE FUNCTION bds.celebration();


--
-- Name: Car_status Car_status_car_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Car_status"
    ADD CONSTRAINT "Car_status_car_id_fkey" FOREIGN KEY (car_id) REFERENCES bds."Company_car"(car_id);


--
-- Name: Customer_contact Customer_contact_customer_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer_contact"
    ADD CONSTRAINT "Customer_contact_customer_id_fkey" FOREIGN KEY (customer_id) REFERENCES bds."Customer"(customer_id);


--
-- Name: Customer_has_adress Customer_has_adress_adress_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer_has_adress"
    ADD CONSTRAINT "Customer_has_adress_adress_id_fkey" FOREIGN KEY (adress_id) REFERENCES bds.adress(adress_id);


--
-- Name: Customer_has_adress Customer_has_adress_customer_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer_has_adress"
    ADD CONSTRAINT "Customer_has_adress_customer_id_fkey" FOREIGN KEY (customer_id) REFERENCES bds."Customer"(customer_id);


--
-- Name: Customer_has_order Customer_has_order_order_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer_has_order"
    ADD CONSTRAINT "Customer_has_order_order_id_fkey" FOREIGN KEY (order_id) REFERENCES bds."Order"(order_id);


--
-- Name: Customer_has_order Customer_has_order_product_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer_has_order"
    ADD CONSTRAINT "Customer_has_order_product_id_fkey" FOREIGN KEY (product_id) REFERENCES bds."Product"(product_id);


--
-- Name: Employee_has_training Employee_has_training_employee_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Employee_has_training"
    ADD CONSTRAINT "Employee_has_training_employee_id_fkey" FOREIGN KEY (employee_id) REFERENCES bds."Employee"(employee_id);


--
-- Name: Employee_has_training Employee_has_training_training_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Employee_has_training"
    ADD CONSTRAINT "Employee_has_training_training_id_fkey" FOREIGN KEY (training_id) REFERENCES bds."Employee_training"(training_id);


--
-- Name: Employee_role Employee_role_employee_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Employee_role"
    ADD CONSTRAINT "Employee_role_employee_id_fkey" FOREIGN KEY (employee_id) REFERENCES bds."Employee"(employee_id);


--
-- Name: Employee_role Employee_role_role_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Employee_role"
    ADD CONSTRAINT "Employee_role_role_id_fkey" FOREIGN KEY (role_id) REFERENCES bds."Role"(role_id);


--
-- Name: Manager_Employees Manager_Employees_employee_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Manager_Employees"
    ADD CONSTRAINT "Manager_Employees_employee_id_fkey" FOREIGN KEY (employee_id) REFERENCES bds."Employee"(employee_id);


--
-- Name: Manager_Employees Manager_Employees_manager_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Manager_Employees"
    ADD CONSTRAINT "Manager_Employees_manager_id_fkey" FOREIGN KEY (manager_id) REFERENCES bds."Manager"(manager_id);


--
-- Name: Manager Manager_car_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Manager"
    ADD CONSTRAINT "Manager_car_id_fkey" FOREIGN KEY (car_id) REFERENCES bds."Company_car"(car_id);


--
-- Name: Manager_has_role Manager_has_role_manager_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Manager_has_role"
    ADD CONSTRAINT "Manager_has_role_manager_id_fkey" FOREIGN KEY (manager_id) REFERENCES bds."Manager"(manager_id);


--
-- Name: Manager_has_role Manager_has_role_role_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Manager_has_role"
    ADD CONSTRAINT "Manager_has_role_role_id_fkey" FOREIGN KEY (role_id) REFERENCES bds."Role"(role_id);


--
-- Name: Order Order_customer_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Order"
    ADD CONSTRAINT "Order_customer_id_fkey" FOREIGN KEY (customer_id) REFERENCES bds."Customer"(customer_id);


--
-- Name: Vyjezd Vyjezd_car_id_fkey; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Vyjezd"
    ADD CONSTRAINT "Vyjezd_car_id_fkey" FOREIGN KEY (car_id) REFERENCES bds."Company_car"(car_id);


--
-- Name: Customer_contact customer_id; Type: FK CONSTRAINT; Schema: bds; Owner: postgres
--

ALTER TABLE ONLY bds."Customer_contact"
    ADD CONSTRAINT customer_id FOREIGN KEY (customer_id) REFERENCES bds."Customer"(customer_id) ON DELETE CASCADE;


--
-- Name: Manager_pay Manager_pay_manager_id_fkey; Type: FK CONSTRAINT; Schema: bds-private; Owner: postgres
--

ALTER TABLE ONLY "bds-private"."Manager_pay"
    ADD CONSTRAINT "Manager_pay_manager_id_fkey" FOREIGN KEY (manager_id) REFERENCES "bds-private"."Manager"(manager_id);


--
-- Name: SCHEMA bds; Type: ACL; Schema: -; Owner: postgres
--

GRANT USAGE ON SCHEMA bds TO "user";


--
-- Name: TABLE "Car_checkout"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Car_checkout" TO "user";


--
-- Name: TABLE "Car_status"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Car_status" TO "user";


--
-- Name: TABLE "Company_car"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Company_car" TO "user";


--
-- Name: TABLE "Customer"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Customer" TO "user";


--
-- Name: TABLE "Customer_contact"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Customer_contact" TO "user";


--
-- Name: TABLE "Customer_has_adress"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Customer_has_adress" TO "user";


--
-- Name: TABLE "Customer_has_order"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Customer_has_order" TO "user";


--
-- Name: TABLE "Employee"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE bds."Employee" TO teacher;
GRANT SELECT ON TABLE bds."Employee" TO "user";


--
-- Name: TABLE "Employee_has_training"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Employee_has_training" TO "user";


--
-- Name: TABLE "Employee_role"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Employee_role" TO "user";


--
-- Name: TABLE "Employee_training"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Employee_training" TO "user";


--
-- Name: TABLE "Order"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Order" TO "user";


--
-- Name: TABLE "Product"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Product" TO "user";


--
-- Name: TABLE "Everything_from_order"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Everything_from_order" TO "user";


--
-- Name: TABLE "Manager"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Manager" TO "user";


--
-- Name: TABLE "Manager_Employees"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Manager_Employees" TO "user";


--
-- Name: TABLE "Manager_has_role"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Manager_has_role" TO "user";


--
-- Name: TABLE "Role"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Role" TO "user";


--
-- Name: TABLE "Text"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Text" TO student;
GRANT SELECT ON TABLE bds."Text" TO "user";


--
-- Name: TABLE "Vyjezd"; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds."Vyjezd" TO student;
GRANT SELECT ON TABLE bds."Vyjezd" TO "user";


--
-- Name: TABLE adress; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds.adress TO "user";


--
-- Name: TABLE all_orders; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds.all_orders TO "user";


--
-- Name: TABLE number_of_people; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds.number_of_people TO "user";


--
-- Name: TABLE number_of_things; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds.number_of_things TO "user";


--
-- Name: TABLE teachers_view_on_employee; Type: ACL; Schema: bds; Owner: postgres
--

GRANT SELECT ON TABLE bds.teachers_view_on_employee TO teacher;
GRANT SELECT ON TABLE bds.teachers_view_on_employee TO "user";


--
-- Name: Everything_from_order; Type: MATERIALIZED VIEW DATA; Schema: bds; Owner: postgres
--

REFRESH MATERIALIZED VIEW bds."Everything_from_order";


--
-- PostgreSQL database dump complete
--

