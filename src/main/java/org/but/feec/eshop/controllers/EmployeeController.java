package org.but.feec.eshop.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.eshop.App;
import org.but.feec.eshop.api.EmployeeBasicView;
import org.but.feec.eshop.api.EmployeeDetailView;
import org.but.feec.eshop.data.EmployeeRepository;
import org.but.feec.eshop.exceptions.ExceptionHandler;
import org.but.feec.eshop.services.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class EmployeeController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    @FXML
    private TableColumn<EmployeeBasicView, String> email;

    @FXML
    private TableColumn<EmployeeBasicView, String> fname;

    @FXML
    private TableColumn<EmployeeBasicView, Long> id;

    @FXML
    private TableColumn<EmployeeBasicView, String> lname;

    @FXML
    private TableColumn<EmployeeBasicView, String> phone;

    @FXML
    private TableView<EmployeeBasicView> EmployeeBasicView;

    @FXML
    private TextField carReservationTextField;

    @FXML
    private TextField carReservationNameTextField;

    @FXML
    private TextField reservationSearchField;

    @FXML
    private Label reservationLabel;

    private EmployeeService employeeService;
    private EmployeeRepository employeeRepository;

    public EmployeeController() {
    }

    @FXML
    private void initialize() {
        employeeRepository = new EmployeeRepository();
        employeeService = new EmployeeService(employeeRepository);

        id.setCellValueFactory(new PropertyValueFactory<EmployeeBasicView, Long>("id"));
        phone.setCellValueFactory(new PropertyValueFactory<EmployeeBasicView, String>("phone"));
        email.setCellValueFactory(new PropertyValueFactory<EmployeeBasicView, String>("email"));
        fname.setCellValueFactory(new PropertyValueFactory<EmployeeBasicView, String>("firstName"));
        lname.setCellValueFactory(new PropertyValueFactory<EmployeeBasicView, String>("lastName"));

        ObservableList<EmployeeBasicView> observableEmployeeList = initializeEmployeesData();
        EmployeeBasicView.setItems(observableEmployeeList);

        EmployeeBasicView.getSortOrder().add(id);
        initializeTableViewSelection();


        logger.info("EmployeeController initialized");
    }


    private ObservableList<EmployeeBasicView> initializeEmployeesData() {
        List<EmployeeBasicView> employees = employeeService.getEmployeeBasicView();
        return FXCollections.observableArrayList(employees);
    }

    public void handleAddPersonButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("EmployeesCreate.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("BDS JavaFX Create Employee");
            stage.setScene(scene);

//            Stage stageOld = (Stage) signInButton.getScene().getWindow();
//            stageOld.close();
//
//            stage.getIcons().add(new Image(App.class.getResourceAsStream("logos/vut.jpg")));
//            authConfirmDialog();

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    public void handleRefreshButton(ActionEvent actionEvent) {
        ObservableList<EmployeeBasicView> observableEmployeeList = initializeEmployeesData();
        EmployeeBasicView.setItems(observableEmployeeList);
        EmployeeBasicView.refresh();
        EmployeeBasicView.sort();
    }

    public void handleEditPersonButton(ActionEvent actionEvent) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(App.class.getResource("EmployeeEdit.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 500);
            Stage stage = new Stage();
            stage.setTitle("BDS JavaFX Create Employee");
            stage.setScene(scene);

            stage.show();
        } catch (IOException ex) {
            ExceptionHandler.handleException(ex);
        }
    }

    private void initializeTableViewSelection() {
        MenuItem edit = new MenuItem("Edit employee");
        MenuItem detailedView = new MenuItem("Detailed employee inspection");
        MenuItem delete = new MenuItem("Delete employee");
        edit.setOnAction((ActionEvent event) -> {
            EmployeeBasicView employeeView = EmployeeBasicView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("EmployeeEdit.fxml"));
                Stage stage = new Stage();
                stage.setUserData(employeeView);
                stage.setTitle("BDS JavaFX Edit Employee");

                EmployeeEditController controller = new EmployeeEditController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        detailedView.setOnAction((ActionEvent event) -> {
            EmployeeBasicView employeeView = EmployeeBasicView.getSelectionModel().getSelectedItem();
            try {
                FXMLLoader fxmlLoader = new FXMLLoader();
                fxmlLoader.setLocation(App.class.getResource("EmployeesDetailView.fxml"));
                Stage stage = new Stage();

                Long employeeId = employeeView.getId();
                EmployeeDetailView employeeDetailView = employeeService.getEmployeeDetailView(employeeId);

                stage.setUserData(employeeDetailView);
                stage.setTitle("BDS JavaFX Employees Detailed View");

                EmployeeDetailViewController controller = new EmployeeDetailViewController();
                controller.setStage(stage);
                fxmlLoader.setController(controller);

                Scene scene = new Scene(fxmlLoader.load(), 600, 500);

                stage.setScene(scene);

                stage.show();
            } catch (IOException ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        delete.setOnAction((ActionEvent event) -> {
            EmployeeBasicView employeeView = EmployeeBasicView.getSelectionModel().getSelectedItem();
            try {

                Long employeeId = employeeView.getId();
                employeeRepository.deleteEmployee(Math.toIntExact(employeeId));

                employeeDeletedConfirmation();

            } catch (Exception ex) {
                ExceptionHandler.handleException(ex);
            }
        });

        ContextMenu menu = new ContextMenu();
        menu.getItems().add(edit);
        menu.getItems().addAll(detailedView);
        menu.getItems().add(delete);
        EmployeeBasicView.setContextMenu(menu);
    }

    private void employeeDeletedConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Employee deleted Confirmation");
        alert.setHeaderText("Your Employee was successfully deleted.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }


    public void handleSendCarReservation(ActionEvent actionEvent) {

        String reservation = carReservationTextField.getText();
        String reservationName = carReservationNameTextField.getText();
        carReservationTextField.setText("");
        carReservationNameTextField.setText("");
        EmployeeRepository employeeRepository = new EmployeeRepository();
        employeeRepository.submitReservation(reservation, reservationName);
        reservationConfirmation();
    }

    public void handleRestoreButton(ActionEvent actionEvent) {

        EmployeeRepository employeeRepository = new EmployeeRepository();
        employeeRepository.restoreTable();
        dummyTableRestoredConfirmation();

    }

    private void dummyTableRestoredConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Table restored Confirmation");
        alert.setHeaderText("Your Table was successfully restored.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

    private void reservationConfirmation() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Reservation stored Confirmation");
        alert.setHeaderText("Your Reservation was successfully stored.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }

    public void handleSearchButton(ActionEvent actionEvent) {

        String reservationText = reservationSearchField.getText();
        EmployeeRepository employeeRepository = new EmployeeRepository();
        employeeRepository.searchReservation(reservationText);
        String reservations = "";
        for (String reservation : (new EmployeeRepository()).searchReservation(reservationText)) {
            reservations = reservations + "\n" + reservation;
        }

        reservationLabel.setText(reservations);




    }
}
