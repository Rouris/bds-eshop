package org.but.feec.eshop.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.but.feec.eshop.api.EmployeeBasicView;
import org.but.feec.eshop.api.EmployeeEditView;
import org.but.feec.eshop.data.EmployeeRepository;
import org.but.feec.eshop.services.EmployeeService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class EmployeeEditController {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeEditController.class);

    @FXML
    public Button editPersonButton;
    @FXML
    public TextField idTextField;
    @FXML
    private TextField emailTextField;
    @FXML
    private TextField firstNameTextField;
    @FXML
    private TextField lastNameTextField;
    @FXML
    private TextField phoneTextField;

    private EmployeeService employeeService;
    private EmployeeRepository employeeRepository;
    private ValidationSupport validation;

    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        employeeRepository = new EmployeeRepository();
        employeeService = new EmployeeService(employeeRepository);

        validation = new ValidationSupport();
        validation.registerValidator(idTextField, Validator.createEmptyValidator("The id must not be empty."));
        idTextField.setEditable(false);
        validation.registerValidator(emailTextField, Validator.createEmptyValidator("The email must not be empty."));
        validation.registerValidator(firstNameTextField, Validator.createEmptyValidator("The first name must not be empty."));
        validation.registerValidator(lastNameTextField, Validator.createEmptyValidator("The last name must not be empty."));
        validation.registerValidator(phoneTextField, Validator.createEmptyValidator("The phone must not be empty."));

        editPersonButton.disableProperty().bind(validation.invalidProperty());

        loadPersonsData();

        logger.info("EmployeeEditController initialized");
    }

    private void loadPersonsData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof EmployeeBasicView) {
            EmployeeBasicView employeeBasicView = (EmployeeBasicView) stage.getUserData();
            idTextField.setText(String.valueOf(employeeBasicView.getId()));
            emailTextField.setText(employeeBasicView.getEmail());
            firstNameTextField.setText(employeeBasicView.getFirstName());
            lastNameTextField.setText(employeeBasicView.getLastName());
            phoneTextField.setText(employeeBasicView.getPhone());
        }
    }

    @FXML
    public void handleEditPersonButton(ActionEvent event) {
        Long id = Long.valueOf(idTextField.getText());
        String email = emailTextField.getText();
        String firstName = firstNameTextField.getText();
        String lastName = lastNameTextField.getText();
        String phone = phoneTextField.getText();

        EmployeeEditView employeeEditView = new EmployeeEditView();
        employeeEditView.setId(id);
        employeeEditView.setEmail(email);
        employeeEditView.setFirstName(firstName);
        employeeEditView.setLastName(lastName);
        employeeEditView.setPhone(phone);

        employeeService.editEmployee(employeeEditView);

        personEditedConfirmationDialog();
    }

    private void personEditedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Employee Edited Confirmation");
        alert.setHeaderText("Your employee was successfully edited.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }
}
