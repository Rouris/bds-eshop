package org.but.feec.eshop.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.but.feec.eshop.api.EmployeeDetailView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EmployeeDetailViewController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeDetailViewController.class);

    @FXML
    private TextField idTextField;

    @FXML
    private TextField emailTextField;

    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    private TextField phoneTextField;

    @FXML
    private TextField roleIdTextField;

    @FXML
    private TextField roleTypeTextField;


    public Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    @FXML
    public void initialize() {
        idTextField.setEditable(false);
        emailTextField.setEditable(false);
        firstNameTextField.setEditable(false);
        lastNameTextField.setEditable(false);
        phoneTextField.setEditable(false);
        roleIdTextField.setEditable(false);
        roleTypeTextField.setEditable(false);

        loadPersonsData();

        logger.info("EmployeeDetailViewController initialized");
    }

    private void loadPersonsData() {
        Stage stage = this.stage;
        if (stage.getUserData() instanceof EmployeeDetailView) {
            EmployeeDetailView employeeBasicView = (EmployeeDetailView) stage.getUserData();
            idTextField.setText(String.valueOf(employeeBasicView.getId()));
            emailTextField.setText(employeeBasicView.getMail());
            firstNameTextField.setText(employeeBasicView.getFirstName());
            lastNameTextField.setText(employeeBasicView.getLastName());
            phoneTextField.setText(employeeBasicView.getPhone());
            roleIdTextField.setText(String.valueOf(employeeBasicView.getRoleId()));
            roleTypeTextField.setText(employeeBasicView.getRoleType());
        }
    }
}
