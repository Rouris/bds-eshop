package org.but.feec.eshop.controllers;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.util.Duration;
import org.but.feec.eshop.api.EmployeeCreateView;
import org.but.feec.eshop.data.EmployeeRepository;
import org.but.feec.eshop.services.EmployeeService;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class EmployeeCreateController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeCreateController.class);

    @FXML
    public Button newEmployeeCreatePerson;
    @FXML
    private TextField newEmployeeEmail;

    @FXML
    private TextField newEmployeeFirstName;

    @FXML
    private TextField newEmployeeLastName;

    @FXML
    private TextField newEmployeePhone;

    @FXML
    private TextField newEmployeePwd;

    private EmployeeService employeeService;
    private EmployeeRepository employeeRepository;
    private ValidationSupport validation;

    @FXML
    public void initialize() {
        employeeRepository = new EmployeeRepository();
        employeeService = new EmployeeService(employeeRepository);

        validation = new ValidationSupport();
        validation.registerValidator(newEmployeeEmail, Validator.createEmptyValidator("The email must not be empty."));
        validation.registerValidator(newEmployeeFirstName, Validator.createEmptyValidator("The first name must not be empty."));
        validation.registerValidator(newEmployeeLastName, Validator.createEmptyValidator("The last name must not be empty."));
        validation.registerValidator(newEmployeePhone, Validator.createEmptyValidator("The phone must not be empty."));
        validation.registerValidator(newEmployeePwd, Validator.createEmptyValidator("The password must not be empty."));

        newEmployeeCreatePerson.disableProperty().bind(validation.invalidProperty());

        logger.info("EmployeeCreateController initialized");
    }

    @FXML
    void handleCreateNewEmployee(ActionEvent event) {
        String email = newEmployeeEmail.getText();
        String firstName = newEmployeeFirstName.getText();
        String lastName = newEmployeeLastName.getText();
        String phone = newEmployeePhone.getText();
        String password = newEmployeePwd.getText();

        EmployeeCreateView employeeCreateView = new EmployeeCreateView();
        employeeCreateView.setPwd(password.toCharArray());
        employeeCreateView.setEmail(email);
        employeeCreateView.setFirstName(firstName);
        employeeCreateView.setLastName(lastName);
        employeeCreateView.setPhone(phone);

        employeeService.createEmployee(employeeCreateView);

        personCreatedConfirmationDialog();
    }

    private void personCreatedConfirmationDialog() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Employee Created Confirmation");
        alert.setHeaderText("Your Employee was successfully created.");

        Timeline idlestage = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                alert.setResult(ButtonType.CANCEL);
                alert.hide();
            }
        }));
        idlestage.setCycleCount(1);
        idlestage.play();
        Optional<ButtonType> result = alert.showAndWait();
    }
}
