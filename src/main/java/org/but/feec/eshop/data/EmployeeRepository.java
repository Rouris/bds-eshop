package org.but.feec.eshop.data;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import org.but.feec.eshop.api.*;
import org.but.feec.eshop.config.DataSourceConfig;
import org.but.feec.eshop.exceptions.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeRepository {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeRepository.class);


    public EmployeeAuthView findEmployeeByEmail(String email) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT mail, pwd" +
                             " FROM bds.\"Employee\" e" +
                             " WHERE e.mail = ?")
        ) {
            preparedStatement.setString(1, email);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToEmployeeAuth(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Finding employee by ID with addresses failed.", e);
        }
        return null;
    }

    public EmployeeDetailView findEmployeeDetailedView(Long employeeId) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "select e.employee_id,first_name,last_name, mail, phone, r.role_id, role_type from bds.\"Employee\" e left join bds.\"Employee_role\" r on e.employee_id = r.employee_id left join bds.\"Role\" w on r.role_id = w.role_id where e.employee_id = ?; ")
        ) {
            preparedStatement.setLong(1, employeeId);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    return mapToEmployeeDetailView(resultSet);
                }
            }
        } catch (SQLException e) {
            throw new DataAccessException("Find employee by ID with addresses failed.", e);
        }
        return null;
    }

    private EmployeeAuthView mapToEmployeeAuth(ResultSet rs) throws SQLException {
        EmployeeAuthView employee = new EmployeeAuthView();
        employee.setEmail(rs.getString("mail"));
        employee.setPassword(rs.getString("pwd"));
        return employee;
    }


    public List<EmployeeBasicView> getEmployeeBasicView() {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(
                     "SELECT employee_id, mail, first_name, last_name, phone" +
                             " FROM bds.\"Employee\"");
             ResultSet resultSet = preparedStatement.executeQuery();) {
            List<EmployeeBasicView> employeeBasicViews = new ArrayList<>();
            while (resultSet.next()) {
                employeeBasicViews.add(mapToEmployeeBasicView(resultSet));
            }
            return employeeBasicViews;
        } catch (SQLException e) {
            throw new DataAccessException("Employees basic view could not be loaded.", e);
        }
    }

    private EmployeeBasicView mapToEmployeeBasicView(ResultSet rs) throws SQLException {
        EmployeeBasicView employeeBasicView = new EmployeeBasicView();
        employeeBasicView.setId(rs.getLong("employee_id"));
        employeeBasicView.setEmail(rs.getString("mail"));
        employeeBasicView.setFirstName(rs.getString("first_name"));
        employeeBasicView.setLastName(rs.getString("last_name"));
        employeeBasicView.setPhone(rs.getString("phone"));
        return employeeBasicView;
    }

    private EmployeeDetailView mapToEmployeeDetailView(ResultSet rs) throws SQLException {
        EmployeeDetailView employeeDetailView = new EmployeeDetailView();
        employeeDetailView.setId(rs.getLong("employee_id"));
        employeeDetailView.setMail(rs.getString("mail"));
        employeeDetailView.setFirstName(rs.getString("first_name"));
        employeeDetailView.setLastName(rs.getString("last_name"));
        employeeDetailView.setPhone(rs.getString("phone"));
        employeeDetailView.setRoleId(rs.getLong("role_id"));
        employeeDetailView.setRoleType(rs.getString("role_type"));
        return employeeDetailView;
    }

    public void createEmployee(EmployeeCreateView employeeCreateView) {
        String insertPersonSQL = "INSERT INTO bds.\"Employee\" (mail, first_name, last_name, pwd, phone) VALUES (?,?,?,?,?)";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, employeeCreateView.getEmail());
            preparedStatement.setString(2, employeeCreateView.getFirstName());
            preparedStatement.setString(3, employeeCreateView.getLastName());
            preparedStatement.setString(4, String.valueOf(employeeCreateView.getPwd()));
            preparedStatement.setString(5, employeeCreateView.getPhone());

            int affectedRows = preparedStatement.executeUpdate();

            if (affectedRows == 0) {
                throw new DataAccessException("Creating employee failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating employee failed operation on the database failed.");
        }
    }

    public void editEmployee(EmployeeEditView employeeEditView) {
        String insertPersonSQL = "UPDATE bds.\"Employee\" SET mail = ?, first_name = ?, last_name = ?, phone = ? WHERE employee_id = ?";
        String checkIfExists = "SELECT mail FROM bds.\"Employee\" WHERE employee_id = ?";
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(insertPersonSQL, Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, employeeEditView.getEmail());
            preparedStatement.setString(2, employeeEditView.getFirstName());
            preparedStatement.setString(3, employeeEditView.getLastName());
            preparedStatement.setString(4, employeeEditView.getPhone());
            preparedStatement.setLong(5, employeeEditView.getId());

            try {
                connection.setAutoCommit(false);
                try (PreparedStatement ps = connection.prepareStatement(checkIfExists, Statement.RETURN_GENERATED_KEYS)) {
                    ps.setLong(1, employeeEditView.getId());
                    ps.execute();
                } catch (SQLException e) {
                    throw new DataAccessException("This employee for edit do not exists/was not found.");
                }

                int affectedRows = preparedStatement.executeUpdate();

                if (affectedRows == 0) {
                    throw new DataAccessException("Creating employee failed, no rows affected.");
                }
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
            } finally {
                connection.setAutoCommit(true);
            }
        } catch (SQLException e) {
            throw new DataAccessException("Creating employee failed operation on the database failed.");
        }
    }

    public boolean deleteEmployee(int id) {
        try (Connection connection = DataSourceConfig.getConnection();
             PreparedStatement prpstmt = connection.prepareStatement(
                     "DELETE FROM bds.\"Employee\" " +
                             "WHERE employee_id = ?;"
             )) {
            prpstmt.setInt(1, id);
            prpstmt.executeUpdate();
            return true;
        }
        catch (SQLException e) {

        }
        return false;
    }

    public void submitReservation(String reservation, String reservationName) {
        try (Connection connection = DataSourceConfig.getConnection();
             Statement stmt = connection.createStatement()) {
            String query = String.format("INSERT INTO \"bds-private\".\"Reservation\" (reservationtext,reservationName) " +
                    "VALUES ('%s' , '%s');", reservation, reservationName);
            stmt.execute(query);
        }
        catch (SQLException e) {
            logger.error(String.format("Error message: %s", e.getMessage()));
        }
    }

    public void restoreTable() {
        try (Connection connection = DataSourceConfig.getConnection();
             Statement stmt = connection.createStatement()) {
            String query = String.format("create table \"bds-private\".\"Dummy\"();");
            stmt.execute(query);
        }
        catch (SQLException e) {
            logger.error(String.format("Error message: %s", e.getMessage()));
        }
    }

    public List<String> searchReservation(String reservationText) {
        try (Connection connection = DataSourceConfig.getConnection();
             Statement stmt = connection.createStatement()) {
            String query = String.format("select reservationtext from \"bds-private\".\"Reservation\" where reservationtext = '%s' ;", reservationText);
            ResultSet rs = stmt.executeQuery(query);
            ArrayList<String> reservations = new ArrayList<>();
            while (rs.next()) {
                reservations.add(rs.getString("reservationtext"));
            }
            if (reservations.isEmpty()) reservations.add("Nothing found.");
            return reservations;

        }
        catch (SQLException e) {
            logger.error(String.format("Error message: %s", e.getMessage()));
        }
        return null;
    }


}


