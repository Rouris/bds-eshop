package org.but.feec.eshop.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.eshop.api.EmployeeAuthView;
import org.but.feec.eshop.data.EmployeeRepository;
import org.but.feec.eshop.exceptions.ResourceNotFoundException;

public class AuthService {
    private EmployeeRepository employeeRepository;

    public AuthService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;

    }

    private EmployeeAuthView findEmployeeByEmail(String email) {
        return employeeRepository.findEmployeeByEmail(email);
    }

    public boolean authenticate(String username, String password) {
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }

        EmployeeAuthView employeeAuthView = findEmployeeByEmail(username);
        if (employeeAuthView == null) {
            throw new ResourceNotFoundException("Provided username is not found.");
        }

        BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), employeeAuthView.getPassword());
        return result.verified;
    }

}
