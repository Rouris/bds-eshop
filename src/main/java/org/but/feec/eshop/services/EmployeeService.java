package org.but.feec.eshop.services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import org.but.feec.eshop.api.EmployeeBasicView;
import org.but.feec.eshop.api.EmployeeCreateView;
import org.but.feec.eshop.api.EmployeeDetailView;
import org.but.feec.eshop.api.EmployeeEditView;
import org.but.feec.eshop.data.EmployeeRepository;

import java.util.List;

public class EmployeeService
{
    private EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository){
        this.employeeRepository = employeeRepository;


    }

    public List<EmployeeBasicView> getEmployeeBasicView() {
        return employeeRepository.getEmployeeBasicView();
    }

    public EmployeeDetailView getEmployeeDetailView(Long id) {
        return employeeRepository.findEmployeeDetailedView(id);
    }

    public void createEmployee(EmployeeCreateView employeeCreateView) {
        char[] originalPassword = employeeCreateView.getPwd();
        char[] hashedPassword = hashPassword(originalPassword);
        employeeCreateView.setPwd(hashedPassword);

        employeeRepository.createEmployee(employeeCreateView);
    }

    public void editEmployee(EmployeeEditView employeeEditView) {
        employeeRepository.editEmployee(employeeEditView);
    }


    private char[] hashPassword(char[] password) {
        return BCrypt.withDefaults().hashToChar(12, password);
    }







}
